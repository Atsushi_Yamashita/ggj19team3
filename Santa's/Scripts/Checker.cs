﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BrancheMaster
{
    [System.Serializable]
    public class Checker
    {
        private List<Iiyama.CommondPanel> panelsP1;
        private List<Iiyama.CommondPanel> panelsP2;

        private PlateManager plateManager = null;
        private MasterDatas masterDatas;
        private ScoreManager scoreManager;

        public System.Action<int> setOrder = null;
        public System.Action changeNextColorP1 = null;
        public System.Action changeNextColorP2 = null;
        public System.Func<int, OrderPanel> getFirstOrderPanel = null;

        public void SetScoreManager(ScoreManager manager)
        {
            scoreManager = manager;
        }

        public void SetMaster(MasterDatas data)
        {
            masterDatas = data;
        }

        public void SetPlateManager(PlateManager manager)
        {
            plateManager = manager;
        }

        public void ResetSetPanelP1()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == 1 || i == 3 || i == 5 || i == 7)
                    SetRandPanel1(i);
            }
        }

        public void ResetSetPanelP2()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == 1 || i == 3 || i == 5 || i == 7)
                    SetRandPanel2(i);
            }
        }

        public void SetRandPanel1(int num)
        {
            Debug.Log("SetRandPanel");
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            panelsP1[num].SetOrder(data.OrderId, data.OrderPicture);
        }

        public void SetRandPanel2(int num)
        {
            Debug.Log("SetRandPanel");
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            panelsP2[num].SetOrder(data.OrderId, data.OrderPicture);
        }

        public void PanalReset()
        {
            panelsP1 = new List<Iiyama.CommondPanel>();
            panelsP2 = new List<Iiyama.CommondPanel>();
        }

        public void PanalAdd(Player target, Iiyama.CommondPanel cmd)
        {
            var p = target == Player.P1 ? panelsP1 : panelsP2;
            p.Add(cmd);
        }

        public void Check1(int i)
        {
            if (panelsP1[i].IsWait)
                return;

            var p = getFirstOrderPanel(panelsP1[i].OrderId);
            if (p != null)
            {
                int plus = 0;
                switch (plateManager.Next1PaletP1.NextType)
                {
                    case 0:
                        plus = 0;
                        break;
                    case 1:
                        plus = 1;
                        break;
                    case 2:
                        plus = -1;
                        break;
                }
                plateManager.ChangeNextColorP1();

                p.SetGetText("1");
                scoreManager.PointAdd(Player.P1, p.GetPoint + plus);
                scoreManager.PointPrint(1);
                // 次のお題だし 仮
                setOrder(p.PanelId);

                // 使ったパネル交換
                SetRandPanel1(i);
            }
            else
            {
                //Miss
                panelsP1[i].SetWait();
                plateManager.ChangeNextColorP1();
                if (plateManager.Next1PaletP1.NextType == 2)
                {
                    scoreManager.PointMiss(Player.P1, 1);
                    scoreManager.PointPrint(1);


                }
            }
        }

        public void Check2(int i)
        {
            if (panelsP2[i].IsWait)
                return;

            var p = getFirstOrderPanel(panelsP2[i].OrderId);
            if (p != null)
            {
                int plus = 0;
                switch (plateManager.Next1PaletP2.NextType)
                {
                    case 0:
                        plus = 0;
                        break;
                    case 1:
                        plus = 1;
                        break;
                    case 2:
                        plus = -1;
                        break;
                }
                plateManager.ChangeNextColorP2();

                p.SetGetText("2");

                scoreManager.PointAdd(Player.P2, p.GetPoint + plus);
                scoreManager.PointPrint(2);

                // 次のお題だし 仮
                setOrder(p.PanelId);

                // 使ったパネル交換
                SetRandPanel2(i);
            }
            else
            {
                //Miss
                panelsP2[i].SetWait();
                plateManager.ChangeNextColorP2();

                if (plateManager.Next1PaletP2.NextType == 2)
                {
                    scoreManager.PointMiss(Player.P2, 1);
                    scoreManager.PointPrint(2);
                }
            }
        }
    }
}