﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Iiyama
{
    public class CommondPanel : MonoBehaviour
    {
        public int OrderId;
        public Image OrderImage;
        public Text OrderInputText;

        public bool IsWait = false;

        public void SetInputText(string keyCode)
        {
            OrderInputText.text = keyCode;
        }

        public void SetOrder(int orderNo, string imgName)
        {
            OrderId = orderNo;
            OrderImage.sprite = Resources.Load<Sprite>("OrderPic/" + imgName);
        }

        public void SetWait()
        {
            StartCoroutine(this.Wait());
        }

        private IEnumerator Wait()
        {
            IsWait = true;
            OrderImage.color = new Color(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(0.5f);
            IsWait = false;
            OrderImage.color = Color.white;
        }
    }
}