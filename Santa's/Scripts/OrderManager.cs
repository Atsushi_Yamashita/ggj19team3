﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BrancheMaster
{
    [System.Serializable]
    public class OrderManager
    {

        private List<OrderPanel> orders;
        public GameObject OrderObj;
        public Transform OrderPanelRootTrans;
        private MasterDatas masterDatas;

        public void SetMaster(MasterDatas master)
        {
            masterDatas = master;
        }

        public void InitOrder()
        {
            if (orders != null)
                return;

            orders = new List<OrderPanel>();
            for (int i = 0; i < 4; i++)
            {
                GameObject obj = GameObject.Instantiate(OrderObj, OrderPanelRootTrans);
                obj.SetActive(true);
                OrderPanel cmd = obj.GetComponent<OrderPanel>();
                cmd.PanelId = i;
                orders.Add(cmd);

                //
                SetOrder(i);
            }
        }

        public OrderPanel GerFirstOrderPanel(int orderId)
        {
            return orders.
                Where(e => e.OrderId == orderId && !e.isWait).FirstOrDefault();
        }

        public void SetOrder(int num)
        {
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            // 仮に 1こでテスト
            orders[num].SetOrder(data.OrderId, data.OrderPicture, data.Point);
            RectTransform rect = orders[num].GetComponent<RectTransform>();
            rect.anchoredPosition = Vector2.zero;
            Vector2 lpos = rect.anchoredPosition;
            //lpos.x += UnityEngine.Random.Range(0f, 700f);
            //lpos.y -= UnityEngine.Random.Range(0f, 500f);
            switch (num)
            {
                case 0:
                    lpos.x += 300f;
                    break;
                case 1:
                    lpos.y -= 230f;
                    break;
                case 2:
                    lpos.x += 600f;
                    lpos.y -= 230f;
                    break;
                case 3:
                    lpos.x += 300f;
                    lpos.y -= 460f;
                    break;
            }
            rect.anchoredPosition = lpos;
        }
    }
}
