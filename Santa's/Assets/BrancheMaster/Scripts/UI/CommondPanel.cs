﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrancheMaster
{
    public class CommondPanel : MonoBehaviour
    {
        [SerializeField] private Image OrderImage;
        [SerializeField] private Text OrderInputText;

        public int OrderId { get; private set; }
        public bool IsWait { get; private set; } = false;

        public void SetInputText(string keyCode)
        {
            OrderInputText.text = keyCode;
        }

        public void SetOrder(int orderId, string imgName)
        {
            OrderId = orderId;
            OrderImage.sprite = Resources.Load<Sprite>("OrderPic/" + imgName);
        }

        public void StartWaitCoroutine()
        {
            StartCoroutine(WaitCoroutine());
        }

        private IEnumerator WaitCoroutine()
        {
            IsWait = true;
            OrderImage.color = new Color(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(0.5f);
            IsWait = false;
            OrderImage.color = Color.white;
        }
    }
}