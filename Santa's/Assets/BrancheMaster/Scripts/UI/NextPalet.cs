﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace BrancheMaster
{
    [System.Serializable]
    public class NextPalet : MonoBehaviour
    {
        private Image image;
        public int NextType;
        public Color ImageColor { get { return image.color; } }
        public bool debugPrint = false;

        private void Awake()
        {
            if(debugPrint) Debug.Log(gameObject.name + "");
            image = GetComponent<Image>();
        }

        //public void SetStart(Image img)
        //{
        //    image = img;
        //}

        public void Set(Color color, int type)
        {
            if (image == null) Debug.LogError("Null!!", gameObject);
            image.color = color;
            NextType = type;
        }

        public void Set(NextPalet p)
        {
            image.color = p.ImageColor;
            NextType = p.NextType;
        }
    }
}