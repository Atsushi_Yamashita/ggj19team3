﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ResultPrint : MonoBehaviour
{
    public Text CountDownText;
    public Text PointTextP1;
    public Text PointTextP2;


    public void PrintWin1 () {
        CountDownText.text = "<size=96>WINNER 1P !!!!</size>";
    }

    public void PrintWin2 () {
        CountDownText.text = "<size=96>WINNER 2P !!!!</size>";
    }

    public void PrintDrow () {
        CountDownText.text = "<size=96>Drow....</size>";
    }

    public void ResetPrint () {
        CountDownText.text = "<size=96>Finish!!!!!!</size>";
        PointTextP1.text = "0";
        PointTextP2.text = "0";
    }

}
