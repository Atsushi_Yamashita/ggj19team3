﻿//  MasterDataClass.cs
//  ProductName Santa's
//
//  Created by  on 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Auto Creator MasterDataClass
/// </summary>
public class MasterDataClass
{
    /// <summary>
    /// ステージマスター
    /// </summary>
    /// <param name="id">Id.</param>
    /// <param name="name">Name.</param>
    /// <param name="customerid">CustomerId.</param>
    /// <param name="point">Point.</param>
    [System.Serializable]
    public class MasterStage
    {
        // ステージID
        public int Id;
        // 客名前
        public string Name;
        // 注文画像
        public string CustomerId;
        // 取得ポイント
        public int Point;

        public MasterStage()
        {
        }
        
        public MasterStage(int id, string name, string customerid, int point)
        {
            this.Id = id;
            this.Name = name;
            this.CustomerId = customerid;
            this.Point = point;
        }

        public string ToString()
        {
            return string.Format("Id={0},Name={1},CustomerId={2},Point={3}", Id, Name, CustomerId, Point);
        }
    }

    /// <summary>
    /// 客マスター
    /// </summary>
    /// <param name="stageid">StageId.</param>
    /// <param name="customerid">CustomerId.</param>
    /// <param name="appearancepoints">AppearancePoints.</param>
    [System.Serializable]
    public class MasterStageCustomer
    {
        // 客ID
        public int StageId;
        // 客名前
        public int CustomerId;
        // 出現点数
        public string AppearancePoints;

        public MasterStageCustomer()
        {
        }
        
        public MasterStageCustomer(int stageid, int customerid, string appearancepoints)
        {
            this.StageId = stageid;
            this.CustomerId = customerid;
            this.AppearancePoints = appearancepoints;
        }

        public override string ToString()
        {
            return string.Format("StageId={0},CustomerId={1},AppearancePoints={2}", StageId, CustomerId, AppearancePoints);
        }
    }

    /// <summary>
    /// 客マスター
    /// </summary>
    /// <param name="id">Id.</param>
    /// <param name="name">Name.</param>
    /// <param name="orderid">OrderId.</param>
    /// <param name="orderpicture">OrderPicture.</param>
    /// <param name="outtime">OutTime.</param>
    /// <param name="point">Point.</param>
    [System.Serializable]
    public class MasterCustomer
    {
        // 客ID
        public int Id;
        // 客名前
        public string Name;
        // 注文ID
        public int OrderId;
        // 注文画像
        public string OrderPicture;
        // 退席時間
        public int OutTime;
        // 取得ポイント
        public int Point;

        public MasterCustomer()
        {
        }
        
        public MasterCustomer(int id, string name, int orderid, string orderpicture, int outtime, int point)
        {
            this.Id = id;
            this.Name = name;
            this.OrderId = orderid;
            this.OrderPicture = orderpicture;
            this.OutTime = outtime;
            this.Point = point;
        }

        public string ToString()
        {
            return string.Format("Id={0},Name={1},OrderId={2},OrderPicture={3},OutTime={4},Point={5}", Id, Name, OrderId, OrderPicture, OutTime, Point);
        }
    }
}