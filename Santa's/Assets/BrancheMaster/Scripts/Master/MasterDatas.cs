﻿//  MasterDatas.cs
//  ProductName Santa's
//
//  Created by  on 
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// Auto Creator MasterDataClass
/// </summary>
public class MasterDatas
{
    public List<MasterDataClass.MasterStage> MasterStages;
    public List<MasterDataClass.MasterStageCustomer> MasterStageCustomers;
    public List<MasterDataClass.MasterCustomer> MasterCustomers;

#if UNITY_EDITOR
    /// <summary>
    /// Unity上 Jsonロードテスト
    /// </summary>
    public void LoadLocalAllJson()
    {
        string path = Application.dataPath + "/ILib/MasterData.txt";
        System.IO.StreamReader pStreamReader = new System.IO.StreamReader(path, System.Text.Encoding.GetEncoding("utf-8"));
        string json = pStreamReader.ReadToEnd();
        Debug.Log("json:" + json);
        MasterDatas data = JsonUtility.FromJson<MasterDatas>(json);

        MasterStages = data.MasterStages;
        MasterStageCustomers = data.MasterStageCustomers;
        MasterCustomers = data.MasterCustomers;

    }
#endif
}