﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

namespace BrancheMaster {
    [System.Serializable]
    public class ScoreManager {
        private Point pointP1 = new Point();
        private Point pointP2 = new Point();
        public Text PointTextP1;
        public Text PointTextP2;
        public Text CountDownText;
        private IsStart isStart = null;

        [SerializeField] UnityEvent finish = new UnityEvent();
        [SerializeField] UnityEvent toDraw = new UnityEvent();
        [SerializeField] UnityEvent toWin1 = new UnityEvent();
        [SerializeField] UnityEvent toWin2 = new UnityEvent();


        public void SetIsStart ( IsStart start ) {
            isStart = start;
        }


        public void PointAdd ( Player p, int num ) {
            var t = p == Player.P1 ? pointP1 : pointP2;
            t.point += num;
        }

        public void PointMiss ( Player p, int num ) {
            var t = p == Player.P1 ? pointP1 : pointP2;
            t.point -= num;
        }

        public void PointPrint ( int i ) {
            var point = i == 1 ? pointP1 : pointP2;
            var text = i == 1 ? PointTextP1 : PointTextP2;
            text.text = point.point.ToString();

        }

        public IEnumerator CountDown () {
            CountDownText.text = "3";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "2";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "1";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "GO!!!!!";
            isStart.isStarted = true;
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "";
        }

        public void ScoreReset () {
            pointP1.Reset();
            pointP2.Reset();
        }

        public void Finish () {
            finish.Invoke();
            if (pointP1 == pointP2) {
                toDraw.Invoke();
            } else {
                var winner = pointP1.point > pointP2.point ? toWin1 : toWin2;
                winner.Invoke();
            }
        }
    }
}