﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace BrancheMaster {
    public class MasterManager: MonoBehaviour {


        private MasterDatas masterDatas;
        public GameObject PanelObj;

        public Transform PanelP1RootTrans;
        public Transform PanelP2RootTrans;

        private int nextOrderIndex = 0;

        private IsStart isStarted = new IsStart();
        private bool isCountDown = false;

        private string [] keyStringsP1 = new string []
        {
        "Q","W","E",
        "A","S","D",
        "Z","X","C",
        };
        private string [] keyStringsP2 = new string []
        {
        "Q","↑","E",
        "←","S","→",
        "Z","↓","C",
        };

        [SerializeField] Checker checker = new Checker();
        //[SerializeField] NextPalet nextPalet = new NextPalet();
        [SerializeField] OrderManager orderManager = new OrderManager();
        [SerializeField] ScoreManager scoreManager = new ScoreManager();
        [SerializeField] PlateManager plateManager = new PlateManager();
        [SerializeField] TimeManager time = new TimeManager();

        void Start () {
            string json = Resources.Load<TextAsset>("MasterData").ToString();
            masterDatas = JsonUtility.FromJson<MasterDatas>(json);
            checker.SetMaster(masterDatas);
            orderManager.SetMaster(masterDatas);
            checker.PanelReset();
            //nextPalet.SetStart(GetComponent<Image>());
            scoreManager.SetIsStart(isStarted);
            checker.SetScoreManager(scoreManager);
            checker.SetPlateManager(plateManager);
            checker.setOrder = orderManager.SetOrder;
            checker.getFirstOrderPanel = orderManager.GerFirstOrderPanel;

            for (int i = 0; i < 9; i++) {
                // P1
                GameObject obj = GameObject.Instantiate(PanelObj, PanelP1RootTrans);
                obj.SetActive(true);
                CommondPanel cmd = obj.GetComponent<CommondPanel>();
                if (i == 1 || i == 3 || i == 5 || i == 7) {
                    cmd.SetInputText(keyStringsP1 [i]);
                } else {
                    cmd.SetInputText("");
                }
                checker.PanelAdd(Player.P1, cmd);
                // P2
                obj = GameObject.Instantiate(PanelObj, PanelP2RootTrans);
                obj.SetActive(true);
                cmd = obj.GetComponent<CommondPanel>();
                if (i == 1 || i == 3 || i == 5 || i == 7) {
                    cmd.SetInputText(keyStringsP2 [i]);
                } else {
                    cmd.SetInputText("");
                }
                checker.PanelAdd(Player.P2, cmd);
            }
        }


        void PanelInit () {
            // パネル初期セット
            checker.ResetCommandPanelP1();
            checker.ResetCommandPanelP2();
            time.Setup();
            plateManager.InitNext();
            //isStart = true;
            isCountDown = true;
            StartCoroutine(scoreManager.CountDown());
        }

        private void GameUpdate () {
            if (isStarted.isStarted == false) return;
            time.Update();

            if (time.IsTimeover == false) {
                this.InputAction();
                return;
            } 

            isStarted.isStarted = false;
            isCountDown = false;
            scoreManager.Finish();
        }


        // Update is called once per frame
        void Update () {
            GameUpdate();
            var startTrigger = !isStarted.isStarted && !isCountDown && Input.GetKeyDown(KeyCode.Space);
            if (startTrigger == false) return;
            // Init Start
            orderManager.InitOrderPanel();
            PanelInit();
        }

        public void ScoreReset () {
            scoreManager.ScoreReset();
        }

        void InputAction () {
            //if(Input.GetButtonDown()

            if (Input.anyKeyDown) {
                foreach (KeyCode code in Enum.GetValues(typeof(KeyCode))) {
                    int num1 = -1;
                    int num2 = -1;
                    if (Input.GetKeyDown(code)) {
                        switch (code) {
                            case KeyCode.W:
                                // 三角
                                num1 = 1;
                                break;
                            case KeyCode.A:
                                // 四角
                                num1 = 3;
                                break;
                            case KeyCode.LeftShift:
                                // L, R

                                checker.ResetCommandPanelP1();
                                break;
                            case KeyCode.D:
                                // 丸
                                num1 = 5;
                                break;
                            case KeyCode.X:
                                // バツ
                                num1 = 7;
                                break;
                            //==========================
                            case KeyCode.UpArrow:
                                // バツ
                                num2 = 1;
                                break;
                            case KeyCode.LeftArrow:
                                num2 = 3;
                                break;
                            case KeyCode.RightArrow:
                                num2 = 5;
                                break;
                            case KeyCode.DownArrow:
                                num2 = 7;
                                break;
                            case KeyCode.RightShift:
                                // L, R
                                checker.ResetCommandPanelP2();
                                break;
                        }
                        if (0 <= num1) {
                            checker.CheckP1(num1);
                        }
                        if (0 <= num2) {
                            checker.CheckP2(num2);
                        }
                        break;
                    }
                }
            }
        }
    }
}