﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

namespace BrancheMaster
{
    [System.Serializable]
    public class Checker
    {
        private List<CommondPanel> commandPanelsP1;
        private List<CommondPanel> commandPanelsP2;

        private PlateManager plateManager = null;
        private MasterDatas masterDatas;
        private ScoreManager scoreManager;

        public System.Action<int> setOrder = null;
        public System.Action changeNextColorP1 = null;
        public System.Action changeNextColorP2 = null;
        public System.Func<int, OrderPanel> getFirstOrderPanel = null;

        public void SetScoreManager(ScoreManager manager)
        {
            scoreManager = manager;
        }

        public void SetPlateManager(PlateManager manager)
        {
            plateManager = manager;
        }

        public void SetMaster(MasterDatas data)
        {
            masterDatas = data;
        }

        public void ResetCommandPanelP1()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == 1 || i == 3 || i == 5 || i == 7)
                    SetRandomCommandPanelP1(i);
            }
        }

        public void ResetCommandPanelP2()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == 1 || i == 3 || i == 5 || i == 7)
                    SetRandomCommandPanelP2(i);
            }
        }

        private void SetRandomCommandPanelP1(int num)
        {
            Debug.Log("SetRandomCommandPanelP1");
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            commandPanelsP1[num].SetOrder(data.OrderId, data.OrderPicture);
        }

        private void SetRandomCommandPanelP2(int num)
        {
            Debug.Log("SetRandomCommandPanelP2");
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            commandPanelsP2[num].SetOrder(data.OrderId, data.OrderPicture);
        }

        public void PanelReset()
        {
            commandPanelsP1 = new List<CommondPanel>();
            commandPanelsP2 = new List<CommondPanel>();
        }

        public void PanelAdd(Player target, CommondPanel cmd)
        {
            var p = target == Player.P1 ? commandPanelsP1 : commandPanelsP2;
            p.Add(cmd);
        }

        public void CheckP1(int i)
        {
            if (commandPanelsP1[i].IsWait) return;

            var p = getFirstOrderPanel(commandPanelsP1[i].OrderId);
            if (p != null)
            {
                int plus = 0;
                switch (plateManager.Next1PaletP1.NextType)
                {
                    case 0:
                        plus = 0;
                        break;
                    case 1:
                        plus = 1;
                        break;
                    case 2:
                        plus = -1;
                        break;
                }
                plateManager.ChangeNextColorP1();

                p.SetGetText("1");
                scoreManager.PointAdd(Player.P1, p.GetPoint + plus);
                scoreManager.PointPrint(1);

                // 次のお題だし 仮
                setOrder(p.PanelId);

                // 使ったパネル交換
                SetRandomCommandPanelP1(i);
            }
            else
            {
                //Miss
                commandPanelsP1[i].StartWaitCoroutine();
                plateManager.ChangeNextColorP1();
                if (plateManager.Next1PaletP1.NextType == 2)
                {
                    scoreManager.PointMiss(Player.P1, 1);
                    scoreManager.PointPrint(1);
                }
            }
        }

        public void CheckP2(int i)
        {
            if (commandPanelsP2[i].IsWait) return;

            var p = getFirstOrderPanel(commandPanelsP2[i].OrderId);
            if (p != null)
            {
                int plus = 0;
                switch (plateManager.Next1PaletP2.NextType)
                {
                    case 0:
                        plus = 0;
                        break;
                    case 1:
                        plus = 1;
                        break;
                    case 2:
                        plus = -1;
                        break;
                }
                plateManager.ChangeNextColorP2();

                p.SetGetText("2");

                scoreManager.PointAdd(Player.P2, p.GetPoint + plus);
                scoreManager.PointPrint(2);

                // 次のお題だし 仮
                setOrder(p.PanelId);

                // 使ったパネル交換
                SetRandomCommandPanelP2(i);
            }
            else
            {
                //Miss
                commandPanelsP2[i].StartWaitCoroutine();
                plateManager.ChangeNextColorP2();

                if (plateManager.Next1PaletP2.NextType == 2)
                {
                    scoreManager.PointMiss(Player.P2, 1);
                    scoreManager.PointPrint(2);
                }
            }
        }
    }
}