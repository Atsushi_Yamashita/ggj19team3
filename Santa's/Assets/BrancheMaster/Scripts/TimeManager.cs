﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class TimeManager 
{
    public float FinishTime = 30.0f;
    public Text TimeText;
    private float timer = 0f;
    public bool IsTimeover { get { return timer <= 0; } }

    public void Setup( ) {
        timer = FinishTime;
        TimeText.text = timer.ToString("0.00");
    }

    public void Update () {
        timer -= Time.deltaTime;
        if(IsTimeover) timer = 0f;
        TimeText.text = timer.ToString("0.00");

    }
}
