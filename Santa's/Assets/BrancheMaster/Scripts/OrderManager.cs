﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Serialization;

namespace BrancheMaster
{
    [System.Serializable]
    public class OrderManager
    {
        [SerializeField] private GameObject orderObj;
        [SerializeField] private Transform orderPanelRootTrans;
        [SerializeField] private List<Transform> orderPositions;

        private List<OrderPanel> orderPanels;
        private MasterDatas masterDatas;

        public void SetMaster(MasterDatas master)
        {
            masterDatas = master;
        }

        /// <summary>
        /// OrderPanelを初期化する
        /// </summary>
        public void InitOrderPanel()
        {
            if (orderPanels != null) return;

            orderPanels = new List<OrderPanel>();
            for (int i = 0; i < orderPositions.Count; i++)
            {
                var orderPanelObj = Object.Instantiate(orderObj, orderPanelRootTrans);
                orderPanelObj.SetActive(true);
                OrderPanel orderPanel = orderPanelObj.GetComponent<OrderPanel>();
                orderPanel.PanelId = i;
                orderPanels.Add(orderPanel);

                SetOrder(i);
            }
        }

        public OrderPanel GerFirstOrderPanel(int orderId)
        {
            return orderPanels.Where(e => e.OrderId == orderId && !e.isWait).FirstOrDefault();
        }

        /// <summary>
        /// 客に対するオーダーを表示する
        /// </summary>
        /// <param name="num"></param>
        public void SetOrder(int num)
        {
            // マスターからデータを設定する
            MasterDataClass.MasterCustomer masterCustomer = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];            // 仮に 1こでテスト
            orderPanels[num].SetOrder(masterCustomer.OrderId, masterCustomer.OrderPicture, masterCustomer.Point);

            // 位置をorderPositionsに設定する
            RectTransform rect = orderPanels[num].GetComponent<RectTransform>();
            rect.anchoredPosition = RectTransformUtility.WorldToScreenPoint(Camera.main, orderPositions[num].position);
        }
    }
}
