﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

namespace Iiyama
{
    public class GameSample : MonoBehaviour
    {
        public float FinishTime = 30.0f;
        public Text TimeText;
        private float timer = 0f;
        private int point = 0;
        public Text PointText;

        // Singleton
        public static GameSample Instance;
        private void Awake()
        {
            if (GameSample.Instance == this)
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(this);
        }

        private MasterDatas masterDatas;
        public GameObject PanelObj;
        public Transform PanelRootTrans;
        private List<BrancheMaster.CommondPanel> panels;

        public GameObject OrderObj;
        public Transform OrderPanelRootTrans;
        private List<BrancheMaster.OrderPanel> orders;
        private int nextOrderIndex = 0;

        private bool isStart = false;

        private string[] keyStrings = new string[]
        {
        "Q","W","E",
        "A","S","D",
        "Z","X","C",
        };

        // Start is called before the first frame update
        void Start()
        {
            string json = Resources.Load<TextAsset>("MasterData").ToString();
            masterDatas = JsonUtility.FromJson<MasterDatas>(json);

            panels = new List<BrancheMaster.CommondPanel>();
            for (int i = 0; i < 9; i++)
            {
                GameObject obj = GameObject.Instantiate(PanelObj, PanelRootTrans);
                obj.SetActive(true);
                BrancheMaster.CommondPanel cmd = obj.GetComponent<BrancheMaster.CommondPanel>();
                cmd.SetInputText(keyStrings[i]);
                panels.Add(cmd);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!isStart && Input.GetKeyDown(KeyCode.Space))
            {
                isStart = true;
                // Init Start
                InitOrder();

                // パネル初期セット
                ResetSetPanel();
                //timer = FinishTime;
                //TimeText.text = timer.ToString("0.00");
            }
            if (isStart)
            {
                //timer -= Time.deltaTime;
                //if (timer <= 0)
                //{
                //    timer = 0f;
                //}
                //else
                //{
                this.GameUpdate();
                //}
                //TimeText.text = timer.ToString("0.00");
            }
        }

        void GameUpdate()
        {
            if (Input.anyKeyDown)
            {
                int num = -1;
                foreach (KeyCode code in Enum.GetValues(typeof(KeyCode)))
                {
                    if (Input.GetKeyDown(code))
                    {
                        switch (code)
                        {
                            case KeyCode.Q:
                                num = 0;
                                break;
                            case KeyCode.W:
                                num = 1;
                                break;
                            case KeyCode.E:
                                num = 2;
                                break;
                            case KeyCode.A:
                                num = 3;
                                break;
                            case KeyCode.S:
                                num = 4;
                                break;
                            case KeyCode.D:
                                num = 5;
                                break;
                            case KeyCode.Z:
                                num = 6;
                                break;
                            case KeyCode.X:
                                num = 7;
                                break;
                            case KeyCode.C:
                                num = 8;
                                break;
                            case KeyCode.Space:
                                ResetSetPanel();
                                break;
                        }
                        if (0 <= num)
                        {
                            this.Check(num);
                        }
                        break;
                    }
                }
            }
        }

        public void InitOrder()
        {
            if (orders == null) orders = new List<BrancheMaster.OrderPanel>();
            for (int i = 0; i < 4; i++)
            {
                GameObject obj = GameObject.Instantiate(OrderObj, OrderPanelRootTrans);
                obj.SetActive(true);
                BrancheMaster.OrderPanel cmd = obj.GetComponent<BrancheMaster.OrderPanel>();
                cmd.PanelId = i;
                orders.Add(cmd);

                //
                SetOrder(i);
            }
        }

        public void SetOrder(int num)
        {
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            // 仮に 1こでテスト
            orders[num].SetOrder(data.OrderId, data.OrderPicture, data.Point);
            RectTransform rect = orders[num].GetComponent<RectTransform>();
            rect.anchoredPosition = Vector2.zero;
            Vector2 lpos = rect.anchoredPosition;
            lpos.x += UnityEngine.Random.Range(0f, 700f);
            lpos.y -= UnityEngine.Random.Range(0f, 500f);
            rect.anchoredPosition = lpos;
        }

        private void Check(int i)
        {
            BrancheMaster.OrderPanel p = orders.Where(e => e.OrderId == panels[i].OrderId).FirstOrDefault();
            if (p != null)
            {
                point += p.GetPoint;
                PointText.text = point.ToString();

                // 次のお題だし 仮
                SetOrder(p.PanelId);

                // 使ったパネル交換
                SetRandPanel(i);
            }
        }

        public void ResetSetPanel()
        {
            for (int i = 0; i < 9; i++)
            {
                SetRandPanel(i);
            }
        }

        public void SetRandPanel(int num)
        {
            Debug.Log("SetRandPanel");
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            panels[num].SetOrder(data.OrderId, data.OrderPicture);
        }
    }
}