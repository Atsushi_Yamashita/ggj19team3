﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

namespace Iiyama
{
    public class GameSample3 : MonoBehaviour
    {
        public float FinishTime = 30.0f;
        public Text TimeText;
        private float timer = 0f;
        private int pointP1 = 0;
        public Text PointTextP1;
        private int pointP2 = 0;
        public Text PointTextP2;


        public Text CountDownText;

        // Singleton
        public static GameSample3 Instance;
        private void Awake()
        {
            if (GameSample3.Instance == this)
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(this);
        }

        private MasterDatas masterDatas;
        public GameObject PanelObj;

        public Transform PanelP1RootTrans;
        public Transform PanelP2RootTrans;
        private List<BrancheMaster.CommondPanel> panelsP1;
        private List<BrancheMaster.CommondPanel> panelsP2;

        public GameObject OrderObj;
        public Transform OrderPanelRootTrans;
        private List<BrancheMaster.OrderPanel> orders;
        private int nextOrderIndex = 0;

        private bool isStart = false;
        private bool isCountDown = false;

        private string[] keyStringsP1 = new string[]
        {
        "Q","W","E",
        "A","S","D",
        "Z","X","C",
        };
        private string[] keyStringsP2 = new string[]
        {
        "Q","↑","E",
        "←","S","→",
        "Z","↓","C",
        };

        private Color[] colors = new Color[]
        {
            Color.yellow,
            Color.blue,
            Color.red,
        };
        public BrancheMaster.NextPalet Next1PaletP1;
        public BrancheMaster.NextPalet Next2PaletP1;
        public BrancheMaster.NextPalet Next3PaletP1;
        public BrancheMaster.NextPalet Next4PaletP1;
        public BrancheMaster.NextPalet Next1PaletP2;
        public BrancheMaster.NextPalet Next2PaletP2;
        public BrancheMaster.NextPalet Next3PaletP2;
        public BrancheMaster.NextPalet Next4PaletP2;

        private void ChangeNextColorP1()
        {
            Next1PaletP1.Set(Next2PaletP1);
            Next2PaletP1.Set(Next3PaletP1);
            Next3PaletP1.Set(Next4PaletP1);
            this.SetNext(Next4PaletP1);
        }

        private void ChangeNextColorP2()
        {
            Next1PaletP2.Set(Next2PaletP2);
            Next2PaletP2.Set(Next3PaletP2);
            Next3PaletP2.Set(Next4PaletP2);
            this.SetNext(Next4PaletP2);
        }

        private void InitNext()
        {
            this.SetNext(Next1PaletP1);
            this.SetNext(Next2PaletP1);
            this.SetNext(Next3PaletP1);
            this.SetNext(Next4PaletP1);
            this.SetNext(Next1PaletP2);
            this.SetNext(Next2PaletP2);
            this.SetNext(Next3PaletP2);
            this.SetNext(Next4PaletP2);
        }

        private void SetNext(BrancheMaster.NextPalet p)
        {
            int type = UnityEngine.Random.Range(0, 3);
            p.Set(colors[type], type);
        }

        // Start is called before the first frame update
        void Start()
        {
            string json = Resources.Load<TextAsset>("MasterData").ToString();
            masterDatas = JsonUtility.FromJson<MasterDatas>(json);

            panelsP1 = new List<BrancheMaster.CommondPanel>();
            panelsP2 = new List<BrancheMaster.CommondPanel>();
            for (int i = 0; i < 9; i++)
            {
                // P1
                GameObject obj = GameObject.Instantiate(PanelObj, PanelP1RootTrans);
                obj.SetActive(true);
                BrancheMaster.CommondPanel cmd = obj.GetComponent<BrancheMaster.CommondPanel>();
                if (i == 1 || i == 3 || i == 5 || i == 7)
                {
                    cmd.SetInputText(keyStringsP1[i]);
                }
                else
                {
                    cmd.SetInputText("");
                }
                panelsP1.Add(cmd);
                // P2
                obj = GameObject.Instantiate(PanelObj, PanelP2RootTrans);
                obj.SetActive(true);
                cmd = obj.GetComponent<BrancheMaster.CommondPanel>();
                if (i == 1 || i == 3 || i == 5 || i == 7)
                {
                    cmd.SetInputText(keyStringsP2[i]);
                }
                else
                {
                    cmd.SetInputText("");
                }
                panelsP2.Add(cmd);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!isStart && !isCountDown && Input.GetKeyDown(KeyCode.Space))
            {
                // Init Start
                InitOrder();

                // パネル初期セット
                ResetSetPanelP1();
                ResetSetPanelP2();
                timer = FinishTime;
                TimeText.text = timer.ToString("0.00");
                InitNext();
                //isStart = true;
                isCountDown = true;
                StartCoroutine(this.CountDown());
            }
            if (isStart)
            {
                timer -= Time.deltaTime;
                if (timer <= 0)
                {
                    timer = 0f;
                    isStart = false;
                    isCountDown = false;
                    StartCoroutine(this.Finish());
                }
                else
                {
                    this.GameUpdate();
                }
                TimeText.text = timer.ToString("0.00");
            }
        }

        private IEnumerator Finish()
        {
            yield return null;
            CountDownText.text = "<size=96>Finish!!!!!!</size>";
            yield return new WaitForSeconds(3.0f);
            if (pointP1 == pointP2)
            {
                CountDownText.text = "<size=96>Drow....</size>";
            }
            else if (pointP1 > pointP2)
            {
                CountDownText.text = "<size=96>WINNER 1P !!!!</size>";
            }
            else
            {
                CountDownText.text = "<size=96>WINNER 2P !!!!</size>";
            }
            bool wait = true;
            while (wait)
            {
                if (Input.anyKeyDown)
                {
                    wait = false;
                }
                yield return null;
            }
            pointP1 = 0;
            pointP2 = 0;
            PointTextP1.text = "0";
            PointTextP2.text = "0";
        }

        private IEnumerator CountDown()
        {
            CountDownText.text = "3";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "2";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "1";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "GO!!!!!";
            isStart = true;
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "";
        }

        void GameUpdate()
        {
            //if(Input.GetButtonDown()


            if (Input.anyKeyDown)
            {
                foreach (KeyCode code in Enum.GetValues(typeof(KeyCode)))
                {
                    int num1 = -1;
                    int num2 = -1;
                    if (Input.GetKeyDown(code))
                    {
                        switch (code)
                        {
                            case KeyCode.W:
                                // 三角
                                num1 = 1;
                                break;
                            case KeyCode.A:
                                // 四角
                                num1 = 3;
                                break;
                            case KeyCode.LeftShift:
                                // L, R
                                ResetSetPanelP1();
                                break;
                            case KeyCode.D:
                                // 丸
                                num1 = 5;
                                break;
                            case KeyCode.X:
                                // バツ
                                num1 = 7;
                                break;
                            //==========================
                            case KeyCode.UpArrow:
                                // バツ
                                num2 = 1;
                                break;
                            case KeyCode.LeftArrow:
                                num2 = 3;
                                break;
                            case KeyCode.RightArrow:
                                num2 = 5;
                                break;
                            case KeyCode.DownArrow:
                                num2 = 7;
                                break;
                            case KeyCode.RightShift:
                                // L, R
                                ResetSetPanelP2();
                                break;
                        }
                        if (0 <= num1)
                        {
                            this.Check1(num1);
                        }
                        if (0 <= num2)
                        {
                            this.Check2(num2);
                        }
                        break;
                    }
                }
            }
        }

        public void InitOrder()
        {
            if (orders != null)
                return;

            orders = new List<BrancheMaster.OrderPanel>();
            for (int i = 0; i < 4; i++)
            {
                GameObject obj = GameObject.Instantiate(OrderObj, OrderPanelRootTrans);
                obj.SetActive(true);
                BrancheMaster.OrderPanel cmd = obj.GetComponent<BrancheMaster.OrderPanel>();
                cmd.PanelId = i;
                orders.Add(cmd);

                //
                SetOrder(i);
            }
        }

        public void SetOrder(int num)
        {
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            // 仮に 1こでテスト
            orders[num].SetOrder(data.OrderId, data.OrderPicture, data.Point);
            RectTransform rect = orders[num].GetComponent<RectTransform>();
            rect.anchoredPosition = Vector2.zero;
            Vector2 lpos = rect.anchoredPosition;
            //lpos.x += UnityEngine.Random.Range(0f, 700f);
            //lpos.y -= UnityEngine.Random.Range(0f, 500f);
            switch (num)
            {
                case 0:
                    lpos.x += 300f;
                    break;
                case 1:
                    lpos.y -= 230f;
                    break;
                case 2:
                    lpos.x += 600f;
                    lpos.y -= 230f;
                    break;
                case 3:
                    lpos.x += 300f;
                    lpos.y -= 460f;
                    break;
            }
            rect.anchoredPosition = lpos;
        }

        private void Check1(int i)
        {
            if (panelsP1[i].IsWait)
                return;

            BrancheMaster.OrderPanel p = orders.Where(e => e.OrderId == panelsP1[i].OrderId && !e.isWait).FirstOrDefault();
            if (p != null)
            {
                int plus = 0;
                switch (Next1PaletP1.NextType)
                {
                    case 0:
                        plus = 0;
                        break;
                    case 1:
                        plus = 1;
                        break;
                    case 2:
                        plus = -1;
                        break;
                }
                this.ChangeNextColorP1();

                p.SetGetText("1");
                pointP1 += p.GetPoint + plus;
                PointTextP1.text = pointP1.ToString();

                // 次のお題だし 仮
                SetOrder(p.PanelId);

                // 使ったパネル交換
                SetRandPanel1(i);
            }
            else
            {
                //Miss
                panelsP1[i].StartWaitCoroutine();
                this.ChangeNextColorP1();
                if (Next1PaletP1.NextType == 2)
                {
                    pointP1 -= 1;
                    PointTextP1.text = pointP1.ToString();
                }
            }
        }

        private void Check2(int i)
        {
            if (panelsP2[i].IsWait)
                return;

            BrancheMaster.OrderPanel p = orders.Where(e => e.OrderId == panelsP2[i].OrderId && !e.isWait).FirstOrDefault();
            if (p != null)
            {
                int plus = 0;
                switch (Next1PaletP2.NextType)
                {
                    case 0:
                        plus = 0;
                        break;
                    case 1:
                        plus = 1;
                        break;
                    case 2:
                        plus = -1;
                        break;
                }
                this.ChangeNextColorP2();

                p.SetGetText("2");
                pointP2 += p.GetPoint + plus;
                PointTextP2.text = pointP2.ToString();

                // 次のお題だし 仮
                SetOrder(p.PanelId);

                // 使ったパネル交換
                SetRandPanel2(i);
            }
            else
            {
                //Miss
                panelsP2[i].StartWaitCoroutine();
                this.ChangeNextColorP2();
                if (Next1PaletP2.NextType == 2)
                {
                    pointP2 -= 1;
                    PointTextP2.text = pointP2.ToString();
                }
            }
        }

        public void ResetSetPanelP1()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == 1 || i == 3 || i == 5 || i == 7)
                    SetRandPanel1(i);
            }
        }

        public void ResetSetPanelP2()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == 1 || i == 3 || i == 5 || i == 7)
                    SetRandPanel2(i);
            }
        }

        public void SetRandPanel1(int num)
        {
            Debug.Log("SetRandPanel");
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            panelsP1[num].SetOrder(data.OrderId, data.OrderPicture);
        }

        public void SetRandPanel2(int num)
        {
            Debug.Log("SetRandPanel");
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            panelsP2[num].SetOrder(data.OrderId, data.OrderPicture);
        }
    }
}