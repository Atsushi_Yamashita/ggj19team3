﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System;
using UnityEngine.UI;

namespace Iiyama
{
    public class GameSample2 : MonoBehaviour
    {
        public float FinishTime = 30.0f;
        public Text TimeText;
        private float timer = 0f;
        private int point = 0;
        public Text PointText;

        // Singleton
        public static GameSample2 Instance;
        private void Awake()
        {
            if (GameSample2.Instance == this)
            {
                Destroy(gameObject);
                return;
            }
            DontDestroyOnLoad(this);
        }

        private MasterDatas masterDatas;
        public GameObject PanelObj;
        public Transform PanelRootTrans;
        private List<BrancheMaster.CommondPanel> panels;

        public GameObject OrderObj;
        public Transform OrderPanelRootTrans;
        private List<BrancheMaster.OrderPanel> orders;
        private int nextOrderIndex = 0;

        private bool isStart = false;

        private string[] keyStrings = new string[]
        {
        "Q","W","E",
        "A","S","D",
        "Z","X","C",
        };

        private Color[] colors = new Color[]
        {
            Color.yellow,
            Color.blue,
            Color.red,
        };
        public BrancheMaster.NextPalet Next1Palet;
        public BrancheMaster.NextPalet Next2Palet;
        public BrancheMaster.NextPalet Next3Palet;
        public BrancheMaster.NextPalet Next4Palet;

        private void ChangeNextColor()
        {
            Next1Palet.Set(Next2Palet);
            Next2Palet.Set(Next3Palet);
            Next3Palet.Set(Next4Palet);
            this.SetNext(Next4Palet);
        }

        private void InitNext()
        {
            this.SetNext(Next1Palet);
            this.SetNext(Next2Palet);
            this.SetNext(Next3Palet);
            this.SetNext(Next4Palet);
        }

        private void SetNext(BrancheMaster.NextPalet p)
        {
            int type = UnityEngine.Random.Range(0, 3);
            p.Set(colors[type], type);
        }

        // Start is called before the first frame update
        void Start()
        {
            string json = Resources.Load<TextAsset>("MasterData").ToString();
            masterDatas = JsonUtility.FromJson<MasterDatas>(json);

            panels = new List<BrancheMaster.CommondPanel>();
            for (int i = 0; i < 9; i++)
            {
                GameObject obj = GameObject.Instantiate(PanelObj, PanelRootTrans);
                obj.SetActive(true);
                BrancheMaster.CommondPanel cmd = obj.GetComponent<BrancheMaster.CommondPanel>();
                if (i == 1 || i == 3 || i == 5 || i == 7)
                {
                    cmd.SetInputText(keyStrings[i]);
                }
                else
                {
                    cmd.SetInputText("");
                }
                panels.Add(cmd);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (!isStart && Input.GetKeyDown(KeyCode.Space))
            {
                isStart = true;
                // Init Start
                InitOrder();

                // パネル初期セット
                ResetSetPanel();
                //timer = FinishTime;
                //TimeText.text = timer.ToString("0.00");
                InitNext();
            }
            if (isStart)
            {
                //timer -= Time.deltaTime;
                //if (timer <= 0)
                //{
                //    timer = 0f;
                //}
                //else
                //{
                this.GameUpdate();
                //}
                //TimeText.text = timer.ToString("0.00");
            }
        }

        void GameUpdate()
        {
            if (Input.anyKeyDown)
            {
                int num = -1;
                foreach (KeyCode code in Enum.GetValues(typeof(KeyCode)))
                {
                    if (Input.GetKeyDown(code))
                    {
                        switch (code)
                        {
                            case KeyCode.Q:
                                //num = 0;
                                break;
                            case KeyCode.W:
                                // 三角
                                num = 1;
                                break;
                            case KeyCode.E:
                                //num = 2;
                                break;
                            case KeyCode.A:
                                // 四角
                                num = 3;
                                break;
                            case KeyCode.S:
                                //num = 4;
                                // L, R
                                ResetSetPanel();
                                break;
                            case KeyCode.D:
                                // 丸
                                num = 5;
                                break;
                            case KeyCode.Z:
                                //num = 6;
                                break;
                            case KeyCode.X:
                                // バツ
                                num = 7;
                                break;
                            case KeyCode.C:
                                //num = 8;
                                break;
                            case KeyCode.Space:
                                // L, R
                                ResetSetPanel();
                                break;
                        }
                        if (0 <= num)
                        {
                            this.Check(num);
                        }
                        break;
                    }
                }
            }
        }

        public void InitOrder()
        {
            if (orders == null) orders = new List<BrancheMaster.OrderPanel>();
            for (int i = 0; i < 4; i++)
            {
                GameObject obj = GameObject.Instantiate(OrderObj, OrderPanelRootTrans);
                obj.SetActive(true);
                BrancheMaster.OrderPanel cmd = obj.GetComponent<BrancheMaster.OrderPanel>();
                cmd.PanelId = i;
                orders.Add(cmd);

                //
                SetOrder(i);
            }
        }

        public void SetOrder(int num)
        {
            //MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, 4)];
            // 仮に 1こでテスト
            orders[num].SetOrder(data.OrderId, data.OrderPicture, data.Point);
            RectTransform rect = orders[num].GetComponent<RectTransform>();
            rect.anchoredPosition = Vector2.zero;
            Vector2 lpos = rect.anchoredPosition;
            //lpos.x += UnityEngine.Random.Range(0f, 700f);
            //lpos.y -= UnityEngine.Random.Range(0f, 500f);
            switch (num)
            {
                case 0:
                    lpos.x += 300f;
                    break;
                case 1:
                    lpos.y -= 230f;
                    break;
                case 2:
                    lpos.x += 600f;
                    lpos.y -= 230f;
                    break;
                case 3:
                    lpos.x += 300f;
                    lpos.y -= 460f;
                    break;
            }
            rect.anchoredPosition = lpos;
        }

        private void Check(int i)
        {
            BrancheMaster.OrderPanel p = orders.Where(e => e.OrderId == panels[i].OrderId).FirstOrDefault();
            if (p != null)
            {
                int plus = 0;
                switch (Next1Palet.NextType)
                {
                    case 0:
                        plus = 0;
                        break;
                    case 1:
                        plus = 1;
                        break;
                    case 2:
                        plus = -1;
                        break;
                }
                this.ChangeNextColor();

                point += p.GetPoint + plus;
                PointText.text = point.ToString();

                // 次のお題だし 仮
                SetOrder(p.PanelId);

                // 使ったパネル交換
                //SetRandPanel(i);
            }
            else
            {
                //Miss
                this.ChangeNextColor();
                if (Next1Palet.NextType == 2)
                {
                    point -= 1;
                    PointText.text = point.ToString();
                }
            }
        }

        public void ResetSetPanel()
        {
            for (int i = 0; i < 9; i++)
            {
                if (i == 1 || i == 3 || i == 5 || i == 7)
                    SetRandPanel(i);
            }
        }

        public void SetRandPanel(int num)
        {
            Debug.Log("SetRandPanel");
            //MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[UnityEngine.Random.Range(0, masterDatas.MasterCustomers.Count)];
            int i = -1;
            switch (num)
            {
                case 1:
                    i = 0;
                    break;
                case 3:
                    i = 1;
                    break;
                case 5:
                    i = 2;
                    break;
                case 7:
                    i = 3;
                    break;
            }
            MasterDataClass.MasterCustomer data = masterDatas.MasterCustomers[i];
            panels[num].SetOrder(data.OrderId, data.OrderPicture);
        }
    }
}