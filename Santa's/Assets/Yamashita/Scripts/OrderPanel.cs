﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Yamashita
{
    [System.Serializable]
    public class OrderPanel : MonoBehaviour
    {
        public bool isWait = false;

        public int PanelId;

        public int OrderId;
        public int GetPoint;
        public Image OrderImage;

        public Text GetText;

        public void SetOrder(int orderId, string imgName, int point)
        {
            if (isWait)
            {
                StartCoroutine(this.IESetOrder(orderId, imgName, point));
            }
            else
            {
                this.Set(orderId, imgName, point);
            }
        }

        private void Set(int orderId, string imgName, int point)
        {
            Debug.Log("Set:" + imgName);
            OrderId = orderId;
            GetPoint = point;
            OrderImage.sprite = Resources.Load<Sprite>("OrderPic/" + imgName);
        }

        public void SetGetText(string getText)
        {
            GetText.text = getText;
            OrderImage.color = new Color(0.5f, 0.5f, 0.5f);
            isWait = true;
        }

        private IEnumerator IESetOrder(int orderId, string imgName, int point)
        {
            yield return new WaitForSeconds(1.0f);
            GetText.text = "";
            isWait = false;
            OrderImage.color = Color.white;
            this.Set(orderId, imgName, point);
        }
    }
}