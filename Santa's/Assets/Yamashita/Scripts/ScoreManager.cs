﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Yamashita
{
    [System.Serializable]
    public class ScoreManager
    {
        private Point pointP1 = new Point();
        private Point pointP2 = new Point();
        public Text PointTextP1;
        public Text PointTextP2;
        public Text CountDownText;
        private IsStart isStart = null;

        public void SetIsStart(IsStart start)
        {
            isStart = start;
        }


        public void PointAdd(Player p, int num)
        {
            var t = p == Player.P1 ? pointP1 : pointP2;
            t.point += num;
        }

        public void PointMiss(Player p, int num)
        {
            var t = p == Player.P1 ? pointP1 : pointP2;
            t.point -= num;
        }

        public void PointPrint(int i)
        {
            var point = i == 1 ? pointP1 : pointP2;
            var text = i == 1 ? PointTextP1 : PointTextP2;
            text.text = point.point.ToString();

        }

        public IEnumerator CountDown()
        {
            CountDownText.text = "3";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "2";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "1";
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "GO!!!!!";
            isStart.isStart = true;
            yield return new WaitForSeconds(1.0f);
            CountDownText.text = "";
        }

        public IEnumerator Finish()
        {
            yield return null;
            CountDownText.text = "<size=96>Finish!!!!!!</size>";
            yield return new WaitForSeconds(3.0f);
            if (pointP1 == pointP2)
            {
                CountDownText.text = "<size=96>Drow....</size>";
            }
            else if (pointP1.point > pointP2.point)
            {
                CountDownText.text = "<size=96>WINNER 1P !!!!</size>";
            }
            else
            {
                CountDownText.text = "<size=96>WINNER 2P !!!!</size>";
            }
            bool wait = true;
            while (wait)
            {
                if (Input.anyKeyDown)
                {
                    wait = false;
                }
                yield return null;
            }
            pointP1.Reset();
            pointP2.Reset();
            PointTextP1.text = "0";
            PointTextP2.text = "0";
        }
    }
}