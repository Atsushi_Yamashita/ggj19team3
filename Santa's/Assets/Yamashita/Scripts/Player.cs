﻿namespace Yamashita
{
    public class IsStart
    {

        private bool _isStart = false;
        public bool isStart
        {
            set { _isStart = value; }
            get { return _isStart; }
        }
    }

    public class Point
    {
        public int point = 0;
        public void Reset()
        {
            point = 0;
        }
    }

    public enum Player
    {
        P1, P2
    }
}