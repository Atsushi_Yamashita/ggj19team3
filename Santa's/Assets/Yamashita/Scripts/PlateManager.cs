﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Yamashita
{
    [System.Serializable]
    public class PlateManager
    {

        public Yamashita.NextPalet Next1PaletP1;
        public Yamashita.NextPalet Next2PaletP1;
        public Yamashita.NextPalet Next3PaletP1;
        public Yamashita.NextPalet Next4PaletP1;
        public Yamashita.NextPalet Next1PaletP2;
        public Yamashita.NextPalet Next2PaletP2;
        public Yamashita.NextPalet Next3PaletP2;
        public Yamashita.NextPalet Next4PaletP2;


        private Color[] colors = new Color[]
        {
            Color.yellow,
            Color.blue,
            Color.red,
        };


        public void ChangeNextColorP1()
        {
            Next1PaletP1.Set(Next2PaletP1);
            Next2PaletP1.Set(Next3PaletP1);
            Next3PaletP1.Set(Next4PaletP1);
            this.SetNext(Next4PaletP1);
        }

        public void ChangeNextColorP2()
        {
            Next1PaletP2.Set(Next2PaletP2);
            Next2PaletP2.Set(Next3PaletP2);
            Next3PaletP2.Set(Next4PaletP2);
            this.SetNext(Next4PaletP2);
        }

        public void InitNext()
        {
            this.SetNext(Next1PaletP1);
            this.SetNext(Next2PaletP1);
            this.SetNext(Next3PaletP1);
            this.SetNext(Next4PaletP1);
            this.SetNext(Next1PaletP2);
            this.SetNext(Next2PaletP2);
            this.SetNext(Next3PaletP2);
            this.SetNext(Next4PaletP2);
        }
        private void SetNext(Yamashita.NextPalet p)
        {
            int type = UnityEngine.Random.Range(0, 3);
            p.Set(colors[type], type);
        }
    }
}