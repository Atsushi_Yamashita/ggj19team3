﻿namespace Yamashita {
    //  EnumClass.cs
    //  ProductName Santa's
    //
    //  Created by  on 
    using UnityEngine;
    using System.Collections;
    using System.Collections.Generic;

    /// <summary>
    /// Auto Creator EnumClass
    /// </summary>
    public class EnumClass {
        /// シーン
        public enum SceneState {
            /// タイトル
            TITLE = 0,
            /// ゲーム
            GAME,
            /// リザルト
            RISULT,
        }

        /// キー入力
        public enum KEY_TYPE {
            Q_KEY = 0,
            W_KEY,
            E_KEY,
            A_KEY,
            S_KEY,
            D_KEY,
            Z_KEY,
            X_KEY,
        }
    }
}