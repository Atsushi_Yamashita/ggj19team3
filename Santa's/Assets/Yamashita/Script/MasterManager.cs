﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class MasterManager : MonoBehaviour
{

    public float FinishTime = 30.0f;
    public Text TimeText;
    private float timer = 0f;

    // Singleton
    public static MasterManager Instance;
    private void Awake () {
        if (MasterManager.Instance == this) {
            Destroy(gameObject);
            return;
        }
        DontDestroyOnLoad(this);
    }

    private MasterDatas masterDatas;
    public GameObject PanelObj;

    public Transform PanelP1RootTrans;
    public Transform PanelP2RootTrans;

    private int nextOrderIndex = 0;

    private IsStart isStart = new IsStart();
    private bool isCountDown = false;

    private string [] keyStringsP1 = new string []
    {
        "Q","W","E",
        "A","S","D",
        "Z","X","C",
    };
    private string [] keyStringsP2 = new string []
    {
        "Q","↑","E",
        "←","S","→",
        "Z","↓","C",
    };

    [SerializeField] Checker checker = new Checker();
    //[SerializeField] NextPalet nextPalet = new NextPalet();
    [SerializeField] OrderManager orderManager = new OrderManager();
    [SerializeField] ScoreManager scoreManager = new ScoreManager();
    [SerializeField] PlateManager plateManager = new PlateManager();

    void Start () {
        string json = Resources.Load<TextAsset>("MasterData").ToString();
        masterDatas = JsonUtility.FromJson<MasterDatas>(json);
        checker.SetMaster(masterDatas);
        orderManager.SetMaster(masterDatas);
        checker.PanalReset();
        //nextPalet.SetStart(GetComponent<Image>());
        scoreManager.SetIsStart(isStart);
        checker.SetScoreManager(scoreManager);
        checker.SetPlateManager(plateManager);
        checker.setOrder = orderManager.SetOrder;
        checker.getFirstOrderPanel = orderManager.GerFirstOrderPanel;

        for (int i = 0; i < 9; i++) {
            // P1
            GameObject obj = GameObject.Instantiate(PanelObj, PanelP1RootTrans);
            obj.SetActive(true);
            BrancheMaster.CommondPanel cmd = obj.GetComponent<BrancheMaster.CommondPanel>();
            if (i == 1 || i == 3 || i == 5 || i == 7) {
                cmd.SetInputText(keyStringsP1 [i]);
            } else {
                cmd.SetInputText("");
            }
            checker.PanalAdd(Player.P1, cmd);
            // P2
            obj = GameObject.Instantiate(PanelObj, PanelP2RootTrans);
            obj.SetActive(true);
            cmd = obj.GetComponent<BrancheMaster.CommondPanel>();
            if (i == 1 || i == 3 || i == 5 || i == 7) {
                cmd.SetInputText(keyStringsP2 [i]);
            } else {
                cmd.SetInputText("");
            }
            checker.PanalAdd(Player.P2, cmd);
        }
    }


    // Update is called once per frame
    void Update () {
        if (!isStart.isStart && !isCountDown && Input.GetKeyDown(KeyCode.Space)) {
            // Init Start
            orderManager.InitOrder();

            // パネル初期セット
            checker.ResetSetPanelP1();
            checker.ResetSetPanelP2();
            timer = FinishTime;
            TimeText.text = timer.ToString("0.00");
            plateManager.InitNext();
            //isStart = true;
            isCountDown = true;
            StartCoroutine(scoreManager.CountDown());
        }
        if (isStart.isStart) {
            timer -= Time.deltaTime;
            if (timer <= 0) {
                timer = 0f;
                isStart.isStart = false;
                isCountDown = false;
                StartCoroutine(scoreManager.Finish());
            } else {
                this.GameUpdate();
            }
            TimeText.text = timer.ToString("0.00");
        }
    }


    void GameUpdate () {
        //if(Input.GetButtonDown()

        if (Input.anyKeyDown) {
            foreach (KeyCode code in Enum.GetValues(typeof(KeyCode))) {
                int num1 = -1;
                int num2 = -1;
                if (Input.GetKeyDown(code)) {
                    switch (code) {
                        case KeyCode.W:
                            // 三角
                            num1 = 1;
                            break;
                        case KeyCode.A:
                            // 四角
                            num1 = 3;
                            break;
                        case KeyCode.LeftShift:
                            // L, R

                            checker.ResetSetPanelP1();
                            break;
                        case KeyCode.D:
                            // 丸
                            num1 = 5;
                            break;
                        case KeyCode.X:
                            // バツ
                            num1 = 7;
                            break;
                        //==========================
                        case KeyCode.UpArrow:
                            // バツ
                            num2 = 1;
                            break;
                        case KeyCode.LeftArrow:
                            num2 = 3;
                            break;
                        case KeyCode.RightArrow:
                            num2 = 5;
                            break;
                        case KeyCode.DownArrow:
                            num2 = 7;
                            break;
                        case KeyCode.RightShift:
                            // L, R
                            checker.ResetSetPanelP2();
                            break;
                    }
                    if (0 <= num1) {
                        checker.Check1(num1);
                    }
                    if (0 <= num2) {
                        checker.Check2(num2);
                    }
                    break;
                }
            }
        }
    }
}
